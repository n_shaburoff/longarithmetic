#include "Multipltcation.h"
#include <cmath>
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))

LongInt Multiplication::multiply(LongInt fisrt, LongInt second)
{
	LongInt res;
	return res;
}

LongInt Karatsuba::multiply(LongInt first, LongInt second)
{
	string x = first.getStr(), y = second.getStr();
	string sign = "";
	if (x.find("-") == 0 && y.find("-") == string::npos) {
		sign = "-";
		x.erase(0, 1);
	}
	else if (x.find("-") == string::npos && y.find("-") == 0) {
		sign = "-";
		y.erase(0, 1);
	}
	else if (x.find("-") == 0 && y.find("-") == 0) {
		x.erase(0, 1);
		y.erase(0, 1);
	}

	int length = max(x.size(), y.size());
	while (x.size() < length)
		x.insert(0, "0");

	while (y.size() < length)
		y.insert(0, "0");

	if (x.size() <= 2 && y.size() <= 2) {
		string res = to_string(stoll(x) * stoll(y));
		LongInt result = sign + res;
		return result;
	}
	int fh = length / 2;
	int sh = length - length / 2;

	LongInt high1 = x.substr(0, fh);
	LongInt low1 = x.substr(fh, sh);
	LongInt high2 = y.substr(0, fh);
	LongInt low2 = y.substr(fh, sh);

	LongInt z0 = multiply(low1, low2);
	LongInt z2 = multiply(high1, high2);
	LongInt a_b = low1 + high1;
	LongInt c_d = low2 + high2;
	LongInt z1 = multiply(a_b, c_d);
	LongInt buffer = z1 - z2 - z0;

	for (int i = 0; i < 2 * (length - length / 2); i++)
		z2.setStr(z2.getStr().append("0"));
	for (int i = 0; i < (length - length / 2); i++)
		buffer.setStr(buffer.getStr().append("0"));

	LongInt res = z2 + buffer + z0;
	res = isZero(res.getStr());
	res = sign + res.getStr();
	return res;
}

LongInt ToomCook::multiply(LongInt first, LongInt second)
{
	string x = first.getStr(), y = second.getStr();
	string sign = "";
	if (x.find("-") == 0 && y.find("-") == string::npos) {
		sign = "-";
		x.erase(0, 1);
	}
	else if (x.find("-") == string::npos && y.find("-") == 0) {
		sign = "-";
		y.erase(0, 1);
	}
	else if (x.find("-") == 0 && y.find("-") == 0) {
		x.erase(0, 1);
		y.erase(0, 1);
	}
	int length = max(x.size(), y.size());
	while (x.size() < length)
		x.insert(0, "0");

	while (y.size() < length)
		y.insert(0, "0");

	if (x.size() <= 2 && y.size() <= 2) {
		string res = to_string(stoi(x) * stoi(y));
		LongInt result = sign + res;
		return result;
	}

	int i = ceil(x.size() / 3.);
	LongInt m0 = x.substr(x.size() - i, i);
	LongInt m1 = x.substr(x.size() - 2 * i,i);
	LongInt m2 = (x.size() - 2 * i > 0) ? x.substr(0, x.size() - 2*i): "0";

	LongInt n0 = y.substr(x.size() - i, i);
	LongInt n1 = y.substr(x.size() - 2 * i, i);
	LongInt n2 = (y.size() - 2 * i > 0) ? y.substr(0, y.size() - 2*i) : "0";

	LongInt p0 = m0;//p(0)
	LongInt p1 = m0 + m1 + m2;//p(1)
	LongInt abc = m0 - m1;
	LongInt abs = abc + m2;
	LongInt pMinus1 = abs;
	LongInt pMinus2 = m0 - m1*2 + m2*4;//p(-2)
	LongInt pInf = m2;//p(inf)

	LongInt q0 = n0;//p(0)
	LongInt q1 = n0 + n1 + n2;//p(1)
	LongInt qMinus1 = n0 - n1 + n2;//p(-1)
	LongInt qMinus2 = n0 - n1*2 + n2*4;//p(-2)
	LongInt qInf = n2;//p(inf)

	LongInt r0 = multiply(p0, q0);//r(0)
	LongInt r1 = multiply(p1, q1);//r(1)
	LongInt rMinus1 = multiply(pMinus1, qMinus1);//r(-1)
	LongInt rMinus2 = multiply(pMinus2, qMinus2);//r(-2)
	LongInt rInf = multiply(pInf, qInf);//r(inf)

	LongInt r_0 = r0;
	LongInt r_4 = rInf;
	LongInt r_3 = (rMinus2 - r1) / 3;
	LongInt r_1 = (r1 - rMinus1) / 2;
	LongInt r_2 = rMinus1 - r0;
	r_3 = (r_2 - r_3) / 2;
	rInf = rInf* 2;
	r_3 = r_3 + rInf;
	r_2 = r_2 + r_1 - r_4;
	r_1 = r_1 - r_3;


	/*for (int j = 0; j < i; j++)
		r_1.setStr(r_1.getStr().append("0"));
	for (int j = 0; j < 2 * i; j++)
		r_2.setStr(r_2.getStr().append("0"));
	for (int j = 0; j < 3 * i; j++)
		r_3.setStr(r_3.getStr().append("0"));
	for (int j = 0; j < 4 * i; j++)
		r_4.setStr(r_4.getStr().append("0"));*/

	r_1 = addZeros(r_1.getStr(), i, false);
	r_2 = addZeros(r_2.getStr(), 2 * i, false);
	r_3 = addZeros(r_3.getStr(), 3 * i, false);
	r_4 = addZeros(r_4.getStr(), 4 * i, false);


	LongInt res = r_0 + r_1 + r_2 + r_3 + r_4;
	string resultStr = sign + res.getStr();
	res = isZero(resultStr);
	

	return res;
}

LongInt primalityFermat::aModM(string s, unsigned int mod)
{
	unsigned int number = 0;
	for (unsigned int i = 0; i < s.length(); i++)
	{
		// (s[i]-'0') gives the digit value and form 
		// the number 
		number = (number * 10 + (s[i] - '0'));
		number %= mod;
	}
	string res = to_string(number);
	return res;
}

LongInt primalityFermat::ApowBmodM(string& a, unsigned int b, unsigned int m)
{
	// Find a%m 
	LongInt ans = aModM(a, m);
	//unsigned int ans = aModM(a, m);
	LongInt mul = ans;

	// now multiply ans by b-1 times and take 
	// mod with m 
	for (unsigned int i = 1; i < b; i++)
		ans = (ans * mul) % m;

	return ans;
}

LongInt primalityFermat::gcd(LongInt a, LongInt b)
{
	LongInt zero;
	if (a < b)
		return gcd(b, a);
	else if (a % b == zero)
		return b;
	else
		return gcd(b, a % b);

}

bool primalityFermat::isPrime(LongInt n)
{
	LongInt one = 1, four = 4;
	if (n <= one || n == four)
		return false;
	if (n < four)
		return true;
	LongInt rnd, a;
	while (k > 0)
	{
		a = n - 4;
		rnd = rand();
		a = LongInt(2) + rnd % a;

		if (gcd(n, a) != LongInt(1))
			return false;

		if (power(a, n - 1, n) != LongInt(1))
			return false;
		k--;
	}
	return true;

}

LongInt abs(LongInt A)
{
	A.minus = false;
	return A;
}
