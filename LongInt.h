#pragma once
#include <string>
using namespace std;

typedef long long digit;

class Multiplication;

class LongInt {
public:
	LongInt() = default;
	LongInt(string s);
	LongInt(int n);
	static void setMultiplication(Multiplication* m);
	LongInt operator+(const LongInt &other);
	LongInt operator-(const LongInt &other);
	LongInt operator*(const LongInt &other);
	LongInt operator%(const LongInt &other);
	LongInt operator/(const LongInt &other);

	bool minus = false;

	bool operator<(const LongInt& other);
	bool operator==(const LongInt& other);
	bool operator<=(const LongInt& other);
	LongInt operator*(int);
	LongInt operator/(int);
	LongInt operator%(int);

	string getStr() { return str; }
	void setStr(string s) { str = s; }

	string subtract(LongInt lhs, LongInt rhs);

	int len() { return this->str.length(); }
	friend ostream& operator<<(ostream& os, const LongInt& dt);

private:
	
	
	static Multiplication *m;
	string str;
};
string findSum(string str1, string str2);
bool isSmaller(string str1, string str2);
string findDiff(string str1, string str2);
string isZero(string s);
string simpleMultiply(string num1, string num2);
string longDivision(string number, int divisor);
string addZeros(string str, int zeros, bool direction);
string toBin(LongInt a);
LongInt binarydiv(LongInt a, LongInt b);
string substractBinary(string str1, string str2);
string addBinary(string a, string b);
LongInt toDecimal(LongInt bin);
LongInt pow(LongInt base, LongInt power);

