#pragma once
#include <string>
#include "LongInt.h"
class Multiplication {
public:
	virtual LongInt multiply(LongInt fisrt, LongInt second);
};

class Karatsuba : public Multiplication {
public:
	LongInt multiply(LongInt fisrt, LongInt second);
};

class ToomCook : public Multiplication {
public:
	LongInt multiply(LongInt fisrt, LongInt second);
};

class primalityFermat {
private:
	int k = 15;
	LongInt aModM(string s, unsigned int mod);
	LongInt ApowBmodM(string& a, unsigned int b, unsigned int m);
	LongInt gcd(LongInt a, LongInt b);
public:
	bool isPrime(LongInt n);
};


LongInt abs(LongInt A);